// ----------------------------------------------------------------------
// -- COURSE SPECIFI JS:
// ----------------------------------------------------------------------
// -- NOTE: This is where you can add anything you need to do specifically to the course, it will load lastly.
// -- ABOUT: THis file will over-ride everything else, if you need to customize
// -- AUTHOR: You - WDS
// ======================================================================
function passThroughToSF (ltiLocation) {
    callingURL = encodeURIComponent(parent.document.URL);
    ltiURL = encodeURIComponent('http://tool.studyforge.net/lti.php?resource_type=lesson&resource_id=' + ltiLocation);
	
    newlocation = '../../sfframe.htm?loc=' + callingURL + '&ltiloc=' + ltiURL;
    var myWindow = window.open(newlocation);
}


if (typeof isexternal === 'undefined' ) {
	$('.breadcrumbs_course').html(FLVS.settings.course_title);
	$('.breadcrumbs_module').html(FLVS.Sitemap.module[current_module].title);
	$('.breadcrumbs_lesson').html(FLVS.Sitemap.module[current_module].lesson[current_lesson].num+' '+FLVS.Sitemap.module[current_module].lesson[current_lesson].title);
	$('.breadcrumbs_lesson_number').html(FLVS.Sitemap.module[current_module].lesson[current_lesson].num);
}