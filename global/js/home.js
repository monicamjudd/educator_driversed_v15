// ----------------------------------------------------------------------
// -- COURSE SPECIFI JS:
// ----------------------------------------------------------------------
// -- NOTE: This is where you can add anything you need to do specifically to the course homepage, it will load lastly.
// -- ABOUT: THis file will over-ride everything else, if you need to customize
// -- AUTHOR: You - WDS
// ======================================================================
var segments = false;

//If segments is set to true, fill out the following variables
var NumberOFModuleSeg1 = 4;
var NumberOFModuleSeg2 = 4;

		
function createIndex(){
	// JavaScript Document//If the homepage is to be divided into segments, set to true - otherwise, set to false
	
	if (segments == true) {
		$('.seg').show();
	
	} else {
		$('#right_menu_seg1').css({'margin-top': '30px'});
		$('.seg').hide();
	}
	
	var pos = $('#menu_inner').offset();
	$('#nav_menu').css('left', pos.left + 'px');
	

	// Create Popup Menu
	createMenu();
	
	// Create Menu items on the right side of the page
	createRightMenu();
	
	// Create accordion for iPhone version
	createRightMenuMobile();
	
	createAccordians();
	
	handleClicks();

};

function createMenu(){
	var menu = '<div id="menu">';
	menu += '<ul class="nav_menu_modules">';
	
	for(var i=0; i<FLVS.Sitemap.module.length; i++){
		
		if(FLVS.Sitemap.module[i].visible == 'true' || getCookie(settings.course_title + " preview")){
			menu += '<li>';
			menu += '<a href="javascript:void(0);" class="modlink">'+FLVS.Sitemap.module[i].title+'</a>';				
				
				// Lessons
				menu += '<ul class="nav_menu_lessons mod'+(i + 1)+'">';
				var submenu = '';
				
				for(var j=0; j<FLVS.Sitemap.module[i].lesson.length; j++){
					//console.log( FLVS.Sitemap.module[i].lesson[j].page);
					var llink = FLVS.Sitemap.module[i].lesson[j].page[0].href;
					var ltitle = FLVS.Sitemap.module[i].lesson[j].title;
					var lnum = FLVS.Sitemap.module[i].lesson[j].num;
					var lmins = FLVS.Sitemap.module[i].lesson[j].time;
					var lpoints = FLVS.Sitemap.module[i].lesson[j].points;
					llink = llink.replace("../../","");

					submenu += '<li>';
					
					//work around for current theme to allow alternate colors
					var mainWidth = $("main").css("width");
					if(mainWidth > "760px"){
						if(j == 0 || j== 2 || j==4 || j== 6 || j== 8 || j== 10 || j==12 || j==14 || j==16 || j==18 || j==20 || j==22){
							submenu += '<a href="'+llink+'"><span class="lesson_num">'+lnum+'</span>';
						} else {
							submenu += '<a href="'+llink+'" class="odd"><span class="lesson_num">'+lnum+'</span>';
						}
					}else if(mainWidth > "500px"){
						if(j == 0 || j== 2 || j==5 || j== 7 || j== 8 || j== 10 || j==13 || j==15 || j==16 || j==18 || j==21 || j==23){
							submenu += '<a href="'+llink+'"><span class="lesson_num">'+lnum+'</span>';
						} else {
							submenu += '<a href="'+llink+'" class="odd"><span class="lesson_num">'+lnum+'</span>';
						}
					}else{
						if(j%2 == 0){
							submenu += '<a href="'+llink+'"><span class="lesson_num">'+lnum+'</span>';
						} else {
							submenu += '<a href="'+llink+'" class="odd"><span class="lesson_num">'+lnum+'</span>';
						}
					}
					
					
						
					var minutes = "mins";
					if(Number(FLVS.Sitemap.module[i].lesson[j].time) < 2){
						minutes = "min";
					}
					var points = "pts";
					if(Number(FLVS.Sitemap.module[i].lesson[j].points) < 2){
						points = "pt";
					}		
					
					submenu += '<span class="lesson_title">'+ltitle+'</span>';
					//submenu += '<span class="lesson_nfo">'+lmins+' '+minutes+' | '+lpoints+' '+points+'</span></a>';
  					submenu += '</li>';					
					
				}
				
				menu += submenu;
				menu += '</ul>';
				menu += '</li>';
		} // end if visible
		

	}
	
	
// Remove all modlinks from nav_menu_lessons
	$('#nav_menu').append(menu);
	$('.nav_menu_lessons .modlink').remove();
}


function createRightMenu() {
	//Segment 1
	if (segments == true) {
		moduleLength = NumberOFModuleSeg1;
	} else {
		moduleLength = FLVS.Sitemap.module.length - 1;
	}

	var menu = '<ul class="right_menu_modules seg1">';
	for (var i = 1; i <= moduleLength; i++) { //changed 5 to 4
		if (FLVS.Sitemap.module[i].visible == 'true') {
			if(FLVS.Sitemap.module[i].title != 'Finishing the Course'){
				if (i < moduleLength-1) { //changed 5 to 4
					menu += '<li><a href="javascript:void(0);" data="' + FLVS.Sitemap.module[i].title + '" class="right_mod_link bottom_line">' + FLVS.Sitemap.module[i].title + '</a></li>';
				}
	
				//last link in the list
				else {
					menu += '<li><a href="javascript:void(0);" data="' + FLVS.Sitemap.module[i].title + '" class="right_mod_link">' + FLVS.Sitemap.module[i].title + '</a></li>';
				};
			}
		};
	};
	menu += '</ul>';

	// Remove all modlinks from nav_menu_lessons
	$('#right_menu_seg1').append(menu);
	$('.nav_menu_lessons .modlink').remove();

	if (segments == true) {
		moduleLength = NumberOFModuleSeg1 + NumberOFModuleSeg2;
		//Segment 2
		var menu = '<ul class="right_menu_modules seg2">';
		for (var i = (NumberOFModuleSeg1 + 1); i <= moduleLength; i++) {
			if (i < moduleLength) {
	
				menu += '<li><a href="javascript:void(0);" data="' + FLVS.Sitemap.module[i].title + '" class="right_mod_link bottom_line">' + FLVS.Sitemap.module[i].title + '</a></li>';
			}
	
			//last link in the list
			else {
				menu += '<li><a href="javascript:void(0);" data="' + FLVS.Sitemap.module[i].title + '" class="right_mod_link">' + FLVS.Sitemap.module[i].title + '</a></li>';
			};
		};
		menu += '</ul>';

		// Remove all modlinks from nav_menu_lessons
		$('#right_menu_seg2').append(menu);
	};
};

function createRightMenuMobile() {
	//Segment 1
	if (segments == true) {
		moduleLength = NumberOFModuleSeg1;
	} else {
		moduleLength = FLVS.Sitemap.module.length - 1;
	}

	var menu = '';
	for (var i = 1; i <= moduleLength; i++) { //changed 5 to 4

		if (FLVS.Sitemap.module[i].visible == 'true') { // JP added 10/15/2013
		if(FLVS.Sitemap.module[i].title != 'Finishing the Course'){
			if (i < moduleLength-1) { //changed 5 to 4
				menu += '<a class="title mobile_mod right_mod_link bottom_line" href="javascript:void(0);">' + FLVS.Sitemap.module[i].title + '</a>';
			}

			//last link in the list
			else {
				menu += '<a class="title mobile_mod" href="javascript:void(0);">' + FLVS.Sitemap.module[i].title + '</a>';
			}

			menu += '<div class="content">';
			menu += '<ul class="mobile_lesson">';

			for (var j = 0; j < FLVS.Sitemap.module[i].lesson.length; j++) {
				if (j == FLVS.Sitemap.module[i].lesson.length - 1) {

					var links = FLVS.Sitemap.module[i].lesson[0].page[0].href;
					links = links.replace("../../","");

					menu += '<li>';
					menu += '<a href="' + links + '"><span class="lesson_num">' + FLVS.Sitemap.module[i].lesson[j].num + ' </span>';
					menu += FLVS.Sitemap.module[i].lesson[j].title + '</a>';
					menu += '</li>';
				} else {
					var links = FLVS.Sitemap.module[i].lesson[0].page[0].href;
					links = links.replace("../../","");

					menu += '<li>';
					menu += '<a class="bottom_line" href="' + links + '"><span class="lesson_num">' + FLVS.Sitemap.module[i].lesson[j].num + ' </span>';


					menu += FLVS.Sitemap.module[i].lesson[j].title + '</a>';
					menu += '</li>';
				}

			}
			menu += '</ul>';
			menu += '</div>';
		}
		}

	} // JP added 10/15/2013
	// Remove all modlinks from nav_menu_lessons
	$('#right_menu_seg1_mobile').append(menu);

	//Segment 2
	if (segments == true) {
		moduleLength = NumberOFModuleSeg1 + NumberOFModuleSeg2;
		var menu = '';
		for (var i = (NumberOFModuleSeg1 + 1); i <= moduleLength; i++) {
			if (i < moduleLength) {
				menu += '<a class="title mobile_mod right_mod_link bottom_line" href="javascript:void(0);">' + FLVS.Sitemap.module[i].title + '</a>';
			} else {
				menu += '<a class="title mobile_mod" href="javascript:void(0);">' + FLVS.Sitemap.module[i].title + '</a>';
			};

			menu += '<div class="content">';
			menu += '<ul class="mobile_lesson">';

			for (var j = 0; j < FLVS.Sitemap.module[i].lesson.length; j++) {
				if (j == FLVS.Sitemap.module[i].lesson.length - 1) {

					var links = FLVS.Sitemap.module[i].lesson[0].page[0].href;
					links = links.replace("../../","");

					menu += '<li>';
					menu += '<a href="' + links + '"><span class="lesson_num">' + FLVS.Sitemap.module[i].lesson[j].num + ' </span>';
					menu += FLVS.Sitemap.module[i].lesson[j].title + '</a>';
					menu += '</li>';
				} else {
					var links = FLVS.Sitemap.module[i].lesson[0].page[0].href;
					links = links.replace("../../","");

					menu += '<li>';
					menu += '<a class="bottom_line" href="' + links + '"><span class="lesson_num">' + FLVS.Sitemap.module[i].lesson[j].num + ' </span>';


					menu += FLVS.Sitemap.module[i].lesson[j].title + '</a>';
					menu += '</li>';
				};

			};

			menu += '</ul>';
			menu += '</div>';
		};
		// Remove all modlinks from nav_menu_lessons
		$('#right_menu_seg2_mobile').append(menu);
	};

};

function createAccordians() {
	$('.accordion').each(function () {
		$(this).children('.title').attr('href', 'javascript:void(0);');
	});
	$(document).on('click', '.accordion a.title', function () {
		if (!$(this).next('.content').hasClass('active')) {
			$(this).siblings('.content').slideUp().removeClass('active');
			$(this).next('.content').slideToggle(500).addClass('active');
		} else {
			$(this).siblings('.content').slideUp().removeClass('active');
		}
	});

};

function handleClicks() {

	$(window).bind('resize', function() {
		var pos = $('#menu_inner').offset();
		$('#nav_menu').css('left', pos.left + 'px');
		
		$('h3').removeClass('turned_arrow');
		
		$('#submenu').fadeOut('fast');
		
		$('.arrow').fadeOut('fast');
		
		//$('.accordion').accordion( 'option', 'active', false );
		
		if ($('#right_col').data('lastMobileLink')) {
			lastMobileLink = $('#right_col').data('lastMobileLink');
			$(lastMobileLink).removeClass('turned_arrow');
		}
	});
	
	$('.gs').click(function() {
		window.location.href = 'getting_started/lesson01/gs_01_01.htm';
	});
	
	$('.fts').click(function() {
		window.location.href = 'finishing_the_course/lesson01/finish01.htm';
	});
	
	$('.menubtn, .menubtn_mobile').click(function(){
		
		$('.nav_menu_lessons').hide();
				
		if(!$('#nav_menu').is(':visible')){
			$('body').append('<div class="menu_backdrop">&nbsp;</div>');
			$('.menu_backdrop').click(function(){
				$('#nav_menu').fadeToggle('fast');
				$(this).remove();
			});
		} else {
			$('.menu_backdrop').remove();
		}
		var pos = $('#menu_inner').offset();
		$('#nav_menu').css('left', pos.left + 'px');
		$('#nav_menu').fadeToggle('fast');
				
	});
	
	// Event for Showing Menu Lessons
	$('.modlink').click(function() {
		$('.nav_menu_lessons').hide();
		$(this).next().stop().fadeIn('fast');
	});
	
	$('#seg1 .accordion').click(function() {
		$('#seg2 .accordion').accordion( 'option', 'active', false );
	});
	
	$('#seg2 .accordion').click(function() {
		$('#seg1 .accordion').accordion( 'option', 'active', false );
	});
			
	$('.accordion').click(function() {
		$('h3').removeClass('turned_arrow');
		clickedLink = $(this).find('a').attr('id');
		lastMobileLink = '';

		if ($('#right_col').data('lastMobileLink')) {
			lastMobileLink = $('#right_col').data('lastMobileLink');

		} else {
			$(this).addClass('turned_arrow');
		}

		if (lastMobileLink == clickedLink) {
			$(this).removeClass('turned_arrow');
			lastMobileLink = '';
			$('#right_col').data('lastMobileLink', lastMobileLink)
		} else {
			$(this).addClass('turned_arrow');
			lastMobileLink = $(this).find('a').attr('id');
			$('#right_col').data('lastMobileLink', lastMobileLink);
		}
	});
	
	$('.seg1 li').click(function(e) {
		e.stopPropagation();
	
		//gets which index of the link that was clicked and passes to create the correct lesson
		var i = $(this).index() + 1;
		createLessons(i);
	
		//grabs and stores the id of the link and the link as an object
		clickedLink = $(this).find('a').attr('data');
		clickedLinkObject = $(this).find('a');
	
		//if the user isn't clicking on the same link, position and display the lessons
		lastLink = "";
	
		if ($('#right_col').data('lastLink')) {
			lastLink = $('#right_col').data('lastLink');
		}
		if (lastLink == clickedLink) {
			if ($('#submenu').is(":visible")) {} else {
				lessonPos(clickedLink, clickedLinkObject);
			}
		} else {
			lessonPos(clickedLink, clickedLinkObject);
		}
	
		$('#right_col').data('lastLink', clickedLink);
	
	});
	
	$('.seg2 li').click(function(e) {
		e.stopPropagation();
	
		//gets which index of the link that was clicked and passes to create the correct lesson
		var i = $(this).index() + NumberOFModuleSeg1 + 1;
		createLessons(i);
	
		//grabs and stores the id of the link and the link as an object
		clickedLink = $(this).find('a').attr('data');
		clickedLinkObject = $(this).find('a');
	
		//if the user isn't clicking on the same link, position and display the lessons
		lastLink = "";
	
		if ($('#right_col').data('lastLink')) {
			lastLink = $('#right_col').data('lastLink');
		}
		if (lastLink == clickedLink) {
			if ($('#submenu').is(":visible")) {} else {
				lessonPos(clickedLink, clickedLinkObject);
			}
		} else {
			lessonPos(clickedLink, clickedLinkObject);
		}
		$('#right_col').data('lastLink', clickedLink);
	});
	
	$(document).click(function(e) {
		if ($('#submenu').is(":visible")) {
			$('#submenu').fadeToggle("fast");
			$('.arrow').fadeToggle("fast");
		}
	
	});

};

function lessonPos(clickedLink, clickedLinkObject) {
	//console.log($('#submenu').height());
	//$('#submenu').height(0)
	$('#submenu').hide();
	//hide the arrow that points to the mod
	$('.arrow').hide();
	
	linkPosition = clickedLinkObject.offset();
	linkTop = linkPosition.top;
	centerHeight = linkPosition.top - (($('#submenu').height()) / 2) - 17;
	totalHeight = linkTop + $('#submenu').height();
	
	if (centerHeight < 35) {
		centerHeight = 35;
	}
	var linkTopFinal = linkPosition.top - 35;
	
	//if it's going to be place the lesson menu too low, move it up
	/*if (totalHeight > 555 && linkTop >= 35) {
		centerHeight = 35;
	}*/
	
	//position the arrow
	$('.arrow').css({
		'margin-top': linkTopFinal
	});
	
	//set the new margin-top for the submenu
	$('#submenu').css({
		'margin-top': centerHeight
	});
	
	//animate in the submenu and then the arrow
	
	$('#submenu').fadeIn('fast');
	$('.arrow').delay(100).fadeIn('fast');
	//console.log($('#submenu').height());
};


function createLessons(i) {
	var menu = '<ul class="lessons">';

	console.log(FLVS.Sitemap.module[i].lesson.length);

	for (var j = -1; j < FLVS.Sitemap.module[i].lesson.length; j++) {
		if (j == -1) {
			//menu += '<li class="bottom_line"></li>';
		} else {
			var links = FLVS.Sitemap.module[i].lesson[j].page[0].href;
			links = links.replace('../../','');

			menu += '<li>';
			if (FLVS.Sitemap.module[i].lesson[j].num) {
				if(FLVS.Sitemap.module[i].lesson.length > 1 && j != FLVS.Sitemap.module[i].lesson.length-1){
					menu += '<a class="bottom_line" href="' + links + '"><span class="lesson_num">' + FLVS.Sitemap.module[i].lesson[j].num + ' </span>';
				}else{
					menu += '<a href="' + links + '"><span class="lesson_num">' + FLVS.Sitemap.module[i].lesson[j].num + ' </span>';
				}
			} else {
				if(FLVS.Sitemap.module[i].lesson.length > 1 && j != FLVS.Sitemap.module[i].lesson.length-1){
					menu += '<a class="bottom_line" href="' + links + '">';
				}else{
					menu += '<a href="' + links + '">';
				}
			};

			menu += FLVS.Sitemap.module[i].lesson[j].title + '</a>';
			menu += '</li>';
		};
	};

	menu += '</ul>';
	$('#submenu').html(menu);
};

function ifSiteMap() {
	if(FLVS.Sitemap){
		//console.log('found sitemap');
		createIndex();
		//console.log('found sitemap and ran createIndex()');
		$('body').css('visibility','visible').hide();
		$('body').fadeIn(1000);
		
	}else{
		$( document ).ajaxSuccess(function( event, xhr, settings ) {
		  if(settings.url == "global/xml/sitemap.xml" ) {
			//console.log('imported sitemap');
			//console.log("hello - ajaxSuccess");
			createIndex();
			//console.log('imported sitemap and ran createIndex()');
			$('body').css('visibility','visible').hide();
			$('main').fadeIn(1000);
		  }
		});
	};
};

ifSiteMap();